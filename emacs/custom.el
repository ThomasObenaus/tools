
(if (eq system-type 'windows-nt)
  ;; ON Windows
  (progn
    (message "We are on windows")
    ;; path to aspell executable
    ;(add-to-list 'exec-path "D:/tools/Aspell/bin")
    ;(add-to-list 'exec-path "D:/tools/hunspell/bin/")

    ;(setq ispell-program-name (locate-file "hunspell"
    ;      exec-path exec-suffixes 'file-executable-p))

    ;(require 'ispell)

    ;;disable the version control
    (setq vc-handled-backends ())

    ;; to improve speed a little bit
    (setq w32-get-true-file-attributes nil)

    (setq my-working-directory (concat (getenv "HOMEPATH") "/work/misc/notes/"))

    ;; workarround for projectile issue when using projectile under
    ;; windows 10. See: https://github.com/bbatsov/projectile/issues/1302#issuecomment-442856292
    (setq projectile-git-submodule-command nil)
  )

  ;; Not on Windows
  (progn
    (message "We are not on windows")
    (setq my-working-directory "~/work/misc/notes")
  )
)

;; set the default working directory
(setq default-directory my-working-directory)

(setq org-agenda-files 
    (append (directory-files-recursively my-working-directory "org$")))


;; show time and date in the buffer title
(display-time-mode 1)
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)

;; start in followmode
(setq org-agenda-start-with-follow-mode t)

;; define the states for the todo-items
(setq org-todo-keywords '((sequence "TODO(t)" "IN-PROGRESS(p)" "BLOCKED(b@/!)" "|" "DONE(d!)" "CANCELED(c@)")))

;; list of org-mode tags
(setq org-tag-alist
      '(("to_read" . ?t )
      ("urgent" . ?u )))

;; filtering in agenda view for specific tags
(setq org-agenda-custom-commands
      '(("r" "Stuff to read" tags "to_read" nil)
       ("u" "Urgent topics" tags "urgent" nil)))

;; shortcuts
(eval-after-load "flyspell"
  #'(define-key flyspell-mode-map (kbd "C-,") 'org-narrow-to-subtree))      ;; zoom into subtree
(eval-after-load "flyspell"
  #'(define-key flyspell-mode-map (kbd "C--") 'widen))                      ;; widen from subtree-zoom