# Emacs Setup

This project contains my setup/ settings for emacs.

## Shortcuts

- The key `C` is the `Ctrl` key
- The key `M` is the `Alt` key
- The key `SPC` is the `Space` key

| Action                              | Key               |
| ----------------------------------- | ----------------- |
| Show spacemacs keybindings          | SPC ?             |
| Close Emacs                         | C-x C-c           |
| Other window                        | SPC w w           |
| Open agenda mode                    | SPC a o o         |
| Open agenda list for this week      | SPC a o a         |
| Open todo list                      | SPC a o t         |
| Open tags view                      | SPC a o m         |
| Show open buffers                   | SPC b b           |
| Save all files                      | C-c C-s           |
| Save current file                   | C-c s             |
| Enter autocorretion menu (flyspell) | SPC S c           |
| Narrow to subtree                   | C-,               |
| Widen from narrowed subtree         | C--               |
| Fuzzy find file (also create)       | SPC f f           |
| Fuzzy file search                   | SPC p f           |
| Fuzzy text search (grep)            | SPC s p           |
| Switch between projects             | SPC p p           |
| Add tag                             | C-c C-c           |
| Add Schedule                        | C-c C-s           |
| Add Deadline                        | C-c C-d           |
| Cylce through States                | S-left or S-right |
| Cycle through Priorities            | S-up or S-down    |
| Enter Undo Tree (leave with q)      | SPC a u           |
| Close this window                   | C-x 0             |
| Split horizontally                  | C-x 2             |
| Split vertically                    | C-x 3             |

## Run

```bash
# without window support in a console/terminal
emacs --no-window-system

# with debugging during startup enabled
emacs --debug-init
```

## Installation - Emacs

### Linux

#### use ppa (not needed for ubuntu 17.04 and above)

add ppa to ppa:kelleyk/emacs

```bash
sudo add-apt-repository ppa:kelleyk/emacs
sudo apt update
```

install emacs26

```bash
sudo apt-get install emacs26
```

### Windows

See at [MsWindowsInstallation](https://www.emacswiki.org/emacs/MsWindowsInstallation).
Download binary from [gnu/emacs](http://ftp.gnu.org/gnu/emacs/windows/).

To check the installed version type `M-x` version `RET`.

### Install Spacemacs

```bash
# linux
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

# windows
git clone https://github.com/syl20bnr/spacemacs %HOMEPATH%/AppData/Roaming/.emacs.d
```

Then follow the instructions of the installer.

### Spacemacs Configuration

1. Open the `~/.spacemacs` (windows: `%HOME%/AppData/Roaming/.spacemacs`) file and enable the modules: helm, org and spell-checking.
   ![modules](spacemacs_modules.png)

2. Copy `custom.el` to `$HOME` (to `%HOMEPATH%`)

   ```bash
   cp custom.el $HOME/
   ```

3. Load `custom.el` from within `.spacemacs` file.

   - Add the line `(load-file "~/custom.el")` (or `(load-file (concat (getenv "HOMEPATH") "/custom.el"))` at windows) to the `dotspacemacs/user-init` function.
     ![custom.el](spacemacs_custom_el.png)

4. Start Emacs and install needed modules
   - Install `ag`
     - `M-x package-list-packages` and `RET`
     - select `ag` and press `i` then press `x` to install it

#### Specials for windows

##### AG Silver-Searcher

See: https://github.com/ggreer/the_silver_searcher/wiki/Windows

1. Install Scoop
   - See: https://github.com/lukesampson/scoop
   * Open the powershell
   ```bash
   Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
   Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
   ```

##### Flyspell

**// TODO: Complete the steps for hunspell**

Does not work for windows (no binary available). Instead hunspell can be used.
See: https://lists.gnu.org/archive/html/help-gnu-emacs/2014-04/msg00030.html

1. Download the hunspell installation kit from
   - http://sourceforge.net/projects/ezwinports/files
2. Extract the contents of the zipfile to any directory to `D:\tools`
3. Add hunspell to Windows PATH. The folder containing the hunspell.exe (the bin folder).
4. Add further dictionaries to `D:tools\hunspell\share\hunspell`
