# Setup ssh from Linux A to Linux B

## On Linux A
### install openssh-server
```bash
sudo apt-get install openssh-server
```

### Configure ssh server
#### backup config
```bash
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.backup
sudo chmod a-w /etc/ssh/sshd_config.backup
```

#### disable password login
```bash
sudo vim /etc/ssh/sshd_config
```
set ```#PasswordAuthentication yes``` to ```PasswordAuthentication no```

#### disable root login
set ```#PermitRootLogin yes``` to ```PermitRootLogin no```
PermitRootLogin no

### restart ssh
```bash
sudo ssh restart
sudo systemctl restart ssh
```

### show logs
```bash
tail -f /var/log/auth.log
```

### Add ssh-key for client
#### Create a keypair on the client (Linux B)
For how to create a key see [ssh_keys](ssh_keys.md).

#### Add the key (only the public) to the server (Linux A)
paste the content of the *.pub file into the ```~/.ssh/authorized_keys``` file
```bash
cat <public-key> >>  ~/.ssh/authorized_keys
# i.e.
cat id_rsa.pub >>  ~/.ssh/authorized_keys
```

## On Linux B
### connect with Linux B
```bash
ssh -vv <username>@<ip> -i <keyfile>

# where username is a username on the server (Linux A)
# where ip is the ip of the server (Linux A)
# where keyfile is the one whose public part was registered at the server (Linux A)
ssh -vv user@192.168.1.9 -i ~/.ssh/id_rsa
```
