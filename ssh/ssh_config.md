# ease up ssh call with ~/.ssh/config file
```vim ~/.ssh/config```
```bash
Host <alias/shortcut>
    HostName <ip or url>              # equals the long name or ip of the server
    Port <portnumber>                 # equals the -p parameter
    User <username on the server>     # equals the username in front of the @
    IdentityFile <keyfile to be used> # equals the -i paramater

# i.e.
Host laptop
    HostName 192.168.1.9
    Port 3022
    User user
    IdentityFile ~/.ssh/id_rsa
```

With this config the call ```ssh user@192.168.1.9 -i ~/.ssh/id_rsa -p 3022``` is cut to ```ssh laptop```

## define aliases
You can define aliases as well. For example if you are not able to change the call or in case you want to transparently route traffic through an ssh-tunnel (see [ssh_tunnel](ssh_tunnel.md)).

```vim ~/.ssh/config```
```bash
Host <alias>
  Hostname 127.0.0.1                    # use localhost here
  Port <port>                           # use the input-port for the tunnel here
  IdentityFile <keyfile to be used>     # use the identitty-file in case it is needed here as well

# with this rule each ssh-call to gitserver.myprivate.de (on the local machine) will be
# routed to 127.0.0.1:9300.
# In case there is a tunnel configured at 127.0.0.1:9300 the packets will be routed
# through this tunnel.
Host gitserver.myprivate.de
  Hostname 127.0.0.1
  Port 9300
  IdentityFile ~/.ssh/id_rsa
```
