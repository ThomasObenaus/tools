## generate a ssh-key
```bash
ssh-keygen -t <key-algorithm> -b <key-len> -C "<name-info / email-address>"
# i.e.
ssh-keygen -t rsa -b 4096 -C "user@bla.com"
```

## secure your keys
### Set permissions of .ssh directory to 700 (drwx------)
```bash
sudo chmod 700 ~./ssh
```
### Set permissions of public keys to 644 (-rw-r--r--)
```bash
sudo chmod 644 ~./ssh/id_rsa.pub
```

### Set permissions of private keys to 600 (-rw-------)
```bash
sudo chmod 600 ~./ssh/id_rsa
```

## remove entry from known-hosts
```bash
ssh-keygen -R <ip of server>
# i.e.
ssh-keygen -R 192.168.1.9
```
