# Enable ssh-login into linux inside virtual box
Precondition: There is one NAT networkcard enabled for the VM.
You have to add a prot-forwarding rule (forwards 3022 -> 22).
```bash
# myserver is tha name of the VM
# set the rule
VBoxManage modifyvm myserver --natpf1 "ssh,tcp,,3022,,22"
# to check it
VBoxManage showvminfo myserver
```
Note: At windows the VBoxManage.exe can be found at ```C:\Program Files\Oracle\VirtualBox```
Then just install and set up a openssh-server as described above.
