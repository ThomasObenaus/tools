# port forwarding (tunneling)
A ssh tunnel is an ssh-connection between two computers through which specific packets are sent. All the traffic routed trhough this tunnel is encrypted.

## Create a tunnel via command-line
Forwarding packets going out from localhost over a specific port can be done via this command.
```bash
ssh -vv -N -L <local-port>:<destination ip or url>:<destination-port> <username>@<destination ip or url>

# -vv for verbose output
# -N prevents ssh to execute a command -> creates a tunnel and blocks
# -L the rule for local forwarding

# this example would redirect each packet to 127.0.0.1:9300 to 192.168.1.222:80
# which has to be reachable from the host with ip 192.168.1.111 (bastion-host/ jump-station)
ssh -vv -N -L 9300:192.168.1.222:80 user@192.168.1.9

# this example does the same as the example above with the exception that the
# ssh profile 'laptop' is being used
ssh -vv -N -L 9300:192.168.1.222:80 laptop
```

## using .ssh/config to define shortcuts
It is possible to define the tunnel in the .ssh/config to get a simpler (shorter) command that has to be excecuted to create the tunnel.

```vim ~/.ssh/config```

```bash
Host tunnel
  HostName <ip or url>              # equals the long name or ip of the server
  Port <portnumber>                 # equals the -p parameter
  User <username on the server>     # equals the username in front of the @
  IdentityFile <keyfile to be used> # equals the -i paramater
  LocalForward <local-port> <destination ip or url>:<destination-port> # equals -L <local-port>:<destination ip or url>:<destination-port>

# i.e.
Host tunnel
  HostName 192.168.1.9
  Port 3022
  User user
  IdentityFile ~/.ssh/id_rsa
  LocalForward 9300 192.168.1.222:80
```

With this config the call ```ssh -N -L 9300:192.168.1.222:80 user@192.168.1.9``` is cut to ```ssh -N tunnel```
