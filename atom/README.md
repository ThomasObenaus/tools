## Atom

### Keybindings
Just copy the file [keymap.cson](keymap.cson) to ```~/.atom/```

```bash
cd ~
mkdir tools
git clone git@gitlab.com:ThomasObenaus/tools.git tools
cp tools/atom/keymap.cson ~/.atom/
```

### Packages
List of usefull packages for atom. These can be installed through the settings menu of atom.

* highlight selected
  * highlight selected lines
* trailing spaces
  * remove and show trailing white-spaces
* filesize
  * show file-informations
* atom-runner
  * run scripts inside atom
* linter
  * base plugin for other linter plugins
* linter-js-yaml
  * linter for yaml files
* linter-docker
* markdown-scrollsync
* markdown-writer
* markdown-table-editor
* minimap
* plantuml-viewer
* git-timemachine
  * browse through git history
* project-manager
  * manage and list projects (ctrl+shift+p)
* pretty-json
  * beautify json
* language-protobuf
  * syntax highlighting for protobuf
* language-cmake
  * syntax-highlighting for cmake files
* language-docker
* language-gradle
* language-lisp
* language-markdown
* language-plantuml
* go-plus
  * suite for golang development (includes serveral useful packages for go)
* terminal-plus
  * terminal emulation in atom
* atom-beautify
* git-plus
  * support for git

### Settings-General:
* Font Size: 14
* Preferred Line Lenght: 140
* [x] Scroll Past End
* [x] Show Indent Guide
* [x] Exclude VCS Ignored Paths

### Settings-Treeview:
* [x] Hide Ignored Names
* [x] Hide VCS Ignored Names
* [x] Show on Right Side

### Useful:
* ctrl+shif+p -> command palette
* change keybinding of "fuzzy finder" to ctrl-shift-r
* markdown preview toggle
* bookmarks (ctrl+shift+f2 and ctrl+f2)
* grammar selector (ctrl+shift+l), selects the syntax

### Snippets
Just copy the file [snippets.cson](snippets.cson) to ```~/.atom/```

```bash
cd ~
mkdir tools
git clone git@gitlab.com:ThomasObenaus/tools.git tools
cp tools/atom/snippets.cson ~/.atom/
```
