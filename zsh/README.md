# ZSH

## Install zsh

Taken from [Installing-ZSH](https://github.com/robbyrussell/oh-my-zsh/wiki/Installing-ZSH).

```bash
# install
sudo apt-get install zsh
# make it the default shell
chsh -s $(which zsh)
```

## Install oh-my-zsh

- curl has to be installed
  Taken from [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh).

```bash
# install curl (optional)
sudo apt-get install curl

# install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

## Install powerlevel9k theme for zsh

Taken from [powerlevel9k](https://github.com/bhilburn/powerlevel9k).

```bash
# this the option 2 (oh-my-zsh)
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
```

## Install powerline/fonts

Taken from [powerline/fonts](https://github.com/powerline/fonts).

```bash
sudo apt-get install fonts-powerline
```

## Configure the zsh and enable the powerlevel9k theme

Just copy the [.zshrc](.zshrc) into `~/`.

```bash
mkdir -p ~/tools
git clone git@gitlab.com:ThomasObenaus/tools.git tools
cp tools/zsh/.zshrc ~/
source ~/.zshrc
```

## Gnome Shell Theme

With [Gogh](https://github.com/Mayccoll/Gogh) you can easily install custom color themes for the gnome shell.

```bash
# install prequisites
sudo apt-get install dconf-cli

# download and run gogh
wget -O gogh https://git.io/vQgMr && chmod +x gogh && ./gogh && rm gogh
```

This provides a list of all available themes, ready for installation.
![gogh](gogh.png)
There you have to enter the number of the preferred theme followed by a space and press enter.
Then you can select the theme via right click into the terminal to **temporarily enable it**.
![temporary](gogh2.png)
To enable a theme permanently you have to select it in `Edit>Preferences>Profiles` with setting the option `Profile used when launching a new terminal`.
