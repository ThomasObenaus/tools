# Hyper

Is a terminal emulator for windows. See more at https://hyper.is.

## Installation

1. Install hyper using the downloaded installer.
2. Install the cli by opening hyper and then select `Plugins > Install Hyper CLI command in PATH` 
3. Install the following plugins and themes
    ```bash
    hyper i hyper-solarized-light
    hyper i hyper-solarized-dark
    hyper i hyper-launch-menu
    ```

## Config

Just copy the `.hyper.js` to `C:\Users\<username>\AppData\Roaming\Hyper`