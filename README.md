# Tools

This repo contains a collection of usefull tools and how they can be configured.

## Atom

Useful stuff about atom can be found at [atom/README.md](atom/README.md).

## Emacs

Useful stuff about emacs can be found at [emacs/README.md](emacs/README.md).

## Zsh

Useful stuff about zsh can be found at [zsh/README.md](zsh/README.md).

## Ssh

Useful stuff about ssh can be found at [ssh/README.md](ssh/README.md).

## Visual Studio Code

Useful stuff about vscode can be found at [vscode/README.md](vscode/README.md).
