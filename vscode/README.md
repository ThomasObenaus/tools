# Visual Studio Code

## Useful Extensions

| Extension                   | Purpose                                               |
| --------------------------- | ----------------------------------------------------- |
| Ayu                         | Color Theme                                           |
| Better Comments             | Colorized Comments (i.e. HACK, TODOD, ...)            |
| Better TOML                 | Language support for toml files                       |
| CMake                       | Language support for CMake files                      |
| Docker                      | Language support for Docker                           |
| Docker Explorer             | Manage docker containers                              |
| Go                          | Language support for Golang                           |
| Go Test Explorer            | Shows passed and failed golang tests                  |
| HCL                         | Language support for HashiCorp Configuration Language |
| Lisp                        | Language support for Lisp                             |
| Make                        | Language support for Makefiles                        |
| Markdown Preview Enhanced   | Markdown rendering, linting, tables                   |
| markdownlint                | linter                                                |
| Material Icon Theme         | Icons                                                 |
| PlantUML                    | Syntax, preview and export of PlantUML                |
| Prettier - Code formatter   | Code formatter                                        |
| Python                      | Support for python                                    |
| Simple React Snippets       | Code snippets for react                               |
| systemd-unit-file           | Language support for systemd files                    |
| Terraform                   | Support for terraform                                 |
| Vagrant file support        | Language support for vagrant files                    |
| vscode-icons                | Icons                                                 |
| vscode-proto3               | Language support for protobuf                         |
| Code Spell Checker          | Spell Checking (English, German, ..)                  |
| German - Code Spell Checker | Germany dictionary for Code Spell Checker             |
| Dep                         | Go dependency manager (dep) support                   |
| Path Intellisense           | Autocompletion for filenames                          |
| Git Lens                    | Git Blame, Changes, Reverting                         |
| vscode-reveal               | Markdown as revealjs presentation                     |
| ESLint                      | linter for JavaScript                                 |
| Live Server                 | Local server for development with automatic reload    |
| Code Runner                 | Directly run code inside VSC                          |

## Configuration

### Complete Settings

- at `~/.config/Code/User/settings.json`

```json
{
  "workbench.colorTheme": "Solarized Light",
  "editor.renderWhitespace": "all",
  "markdown.extension.toc.githubCompatibility": true,
  "markdown-preview-enhanced.codeBlockTheme": "github.css",
  "[markdown]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "workbench.iconTheme": "material-icon-theme",
  "editor.formatOnSave": true,
  "editor.minimap.enabled": false,
  "terraform.languageServer": {
    "enabled": true,
    "args": []
  },
  "files.watcherExclude": {
    "**/vendor/**": true
  },
  "go.coverOnSingleTest": true,
  "go.coverageDecorator": {
    "type": "gutter",
    "coveredHighlightColor": "rgba(64,128,128,0.5)",
    "uncoveredHighlightColor": "rgba(128,64,64,0.25)",
    "coveredGutterStyle": "blockgreen",
    "uncoveredGutterStyle": "blockred"
  },
  "go.useCodeSnippetsOnFunctionSuggestWithoutType": true,
  "cSpell.userWords": ["sokar"],
  "terraform.indexing": {
    "enabled": false,
    "liveIndexing": false,
    "delay": 500,
    "exclude": [".terraform/**/*", "**/.terraform/**/*"]
  },
  "better-comments.tags": [
    {
      "tag": "FIXME",
      "color": "#000000",
      "strikethrough": false,
      "backgroundColor": "#ff9900"
    },
    {
      "tag": "todo",
      "color": "#000000",
      "strikethrough": false,
      "backgroundColor": "#80b3ed"
    },
    {
      "tag": "HACK",
      "color": "#FFFFFF",
      "strikethrough": false,
      "backgroundColor": "#ef4f3b"
    },
    {
      "tag": "HINT",
      "color": "#9d7fab",
      "strikethrough": false,
      "backgroundColor": "transparent"
    }
  ]
}
```

### Details

- [x] Render Whitespace
- [x] Format on save
- [ ] Minimap
- Color-Theme: Ayu Mirage Bordered
- Icon-theme: vscode-icons

#### Exclude the `vendor` folder from being watched by VSC

```json
"files.watcherExclude": {
    "**/.git/objects/**": true,
    "**/.git/subtree-cache/**": true,
    "**/node_modules/**": true,
    "**/vendor/**": true,
  }
```

#### Go

- [x] Use Code Snippets On Function Suggest Without Type

##### Code Coverage

```json
  "go.coverOnSingleTest": true,
  "go.coverageDecorator": {
      "type": "gutter",
      "coveredHighlightColor": "rgba(64,128,128,0.5)",
      "uncoveredHighlightColor": "rgba(128,64,64,0.25)",
      "coveredGutterStyle": "blockgreen",
      "uncoveredGutterStyle": "blockred"
  }
```

### Keyboard Shortcuts

- _"Go to file ..."_ -> `Ctrl` + `Shift` + `R`
- _"Toggle Side Bar Visibility ..."_ -> `Ctrl` + `Q`
